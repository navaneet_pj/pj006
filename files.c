#include <stdio.h>
int main()
{
    char c;
    FILE *fp;
    fp=fopen("Input.txt","w");
    printf("Enter the values to be stored in the file\n");
    while((c=getchar())!=EOF)
    {
        fputc(c,fp);
    }
    fclose(fp);
    fp=fopen("Input.txt","r");
    printf("\n");
    printf("The file contents are\n");
    while((c=fgetc(fp))!=EOF)
    {
        printf("%c",c);
    }
    printf("\n");
    fclose(fp);

    return 0;
}
