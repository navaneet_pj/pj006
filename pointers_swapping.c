#include<stdio.h>
int swap(int *a ,int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}
int main ()
{
    int a,b;
    printf("ENTER THE NUMBERS TO BE SWAPPED");
    scanf("%d%d",&a,&b);
    swap(&a,&b);
    printf("THE NUMBERS AFTER SWAPPING IS %d AND %d",a,b);
    return 0;
}
