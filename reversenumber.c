#include <stdio.h>

int main()
{
	int r,n,pal=0,x;
	printf("Enter the number");
    scanf("%d",&n);
    x=n;
    while(n!=0)
    {
        r=n%10;
        pal=(pal*10)+r;
        n=n/10;
    }
    
    if(x==pal)
        {
            printf("The given number %d is a palindrome\n",x);
        }
    else
        {
           printf("The given number %d is not a palindrome\n",x);
        }
	return 0;
}
